import requests
import pytest
import uuid
import time


@pytest.mark.parametrize('url,expected_md5,expected_status,timeout', [
    (
        'http://www.bostongene.com/wp-content/uploads/2019/04/br-website-logo_notm.png',
        '86fb1c504d6df93fd5775a334971dc40',
        'done',
        100,
    ),
    (
        'http://not-existing-address.com/some_file.zip',
        None,
        'failed',
        10,
    ),
#    (
#        'https://speed.hetzner.de/100MB.bin',
#        '2f282b84e7e608d5852449ed940bfc51',
#        'done',
#        10000,
#    ),
])
def test_md5service(url, expected_md5, expected_status, timeout):
    r = requests.post('http://127.0.0.1:8000/submit', data={'url': url})

    assert r.status_code == 200
    assert ['id'] == list(r.json().keys())

    task_id = r.json()['id']

    while timeout > 0:
        r = requests.get('http://127.0.0.1:8000/check', params={'id': task_id})
        assert r.status_code == 200
        assert 'status' in r.json()

        status = r.json()['status']
        if status in ['running', 'not found']:
            time.sleep(1)
            timeout -= 1
            continue

        assert status == expected_status
        if status == 'done':
            md5 = r.json()['md5']
            assert expected_md5 == md5
        break


def test_not_found():
    task_id = str(uuid.uuid4())
    r = requests.get('http://127.0.0.1:8000/check', params={'id': task_id})
    assert r.status_code == 200
    assert 'status' in r.json()
    assert r.json()['status'] == 'not found'

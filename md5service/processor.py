import asyncio
import pathlib
import hashlib
import aiohttp
import logging
import aiosmtplib
from email.mime.text import MIMEText

from utils import load_config, setup_logging, cancel_all_tasks
from db import DB


logger = logging.getLogger('md5app')


PROJECT_PATH = pathlib.Path(__file__).absolute().parent.parent
CONFIG_PATH = PROJECT_PATH / 'config' / 'config.yml'


async def task_processor(processor_id, conf, db):
    logger.debug(f'Task processor {processor_id} initialized')
    while True:
        task_id = await db.get_task()
        url, email, _ = await db.get_task_data(task_id)
        logger.debug(f' Worker {processor_id} got task {task_id}')
        md5 = await calculate_md5(url)
        if email:
            text = f'md5: {md5}, url: {url}'
            await send_email(conf, email, text)
        await db.complete_task(task_id, md5)
        logger.debug(f' Worker {processor_id} finished task {task_id}')


async def calculate_md5(url, buffer_size=2**16):
    md5 = hashlib.md5()
    logger.debug(f' Trying to retrieve {url}')
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                if resp.status != 200:
                    logger.debug(f' Failed to retrieve {url}')
                    return '0'
                while True:
                    chunk = await resp.content.read(buffer_size)
                    if not chunk:
                        break
                    md5.update(chunk)
        logger.debug(f' Successfully retrieved {url} and calculated md5 sum')
        return md5.hexdigest()
    except Exception:
        logger.exception('')
        return '0'


async def send_email(conf, email, text):
    logger.debug(f'Sending text "{text}" to {email}')
    message = MIMEText(text)
    message['Subject'] = conf['subject']
    message['From'] = conf['from']
    message['To'] = email

    try:
        await aiosmtplib.send(message,
                              hostname=conf['hostname'],
                              username=conf['username'],
                              password=conf['password'],
                              port=conf['port'],
                              start_tls=True)
        logger.debug(f'Successfully sent text "{text}" to {email}')
    except Exception:
        logger.error(f'Failed to send text "{text}" to {email}')
        logger.exception('')


async def run_processor(conf, db):
    tasks = []
    await db.move_running_to_pending()
    for i in range(conf['size']):
        task = asyncio.create_task(task_processor(i, conf['email'], db))
        tasks.append(task)
    await asyncio.gather(*tasks)


async def init(loop):
    cleaners = []
    conf = load_config(CONFIG_PATH)
    redis_conf = conf['redis']
    processor_conf = conf['processor']

    db = await DB.init(redis_conf, loop)
    cleaners.append(db.close)

    task = asyncio.create_task(run_processor(processor_conf, db))
    return task, cleaners


def main():
    setup_logging()

    loop = asyncio.get_event_loop()
    task, cleaners = loop.run_until_complete(init(loop))

    try:
        loop.run_until_complete(task)
    except (SystemExit, KeyboardInterrupt):
        pass
    finally:
        cancel_all_tasks(loop)
        loop.run_until_complete(asyncio.gather(*[c() for c in cleaners]))
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.stop()


if __name__ == '__main__':
    main()

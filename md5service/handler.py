from aiohttp import web


class Handler(object):
    def __init__(self, db, conf):
        self._db = db
        self._conf = conf

    async def check(self, request):
        task_id = request.rel_url.query.get('id')
        url, _, md5 = await self._db.get_task_data(task_id)

        if not url:
            result = {
                'status': 'not found'
            }
        elif not md5:
            result = {
                'status': 'running'
            }
        elif md5 == '0':
            result = {
                'status': 'failed'
            }
        else:
            result = {
                'status': 'done',
                'url': url,
                'md5': md5
            }
        return web.json_response(result)

    async def submit(self, request):
        data = await request.post()
        url = data.get('url')
        email = data.get('email')
        task_id = await self._db.create_task(url, email)
        result = {
            'id': task_id
        }
        return web.json_response(result)

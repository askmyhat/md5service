import argparse
import asyncio
import pathlib
import logging

from aiohttp import web

from utils import load_config, setup_logging, cancel_all_tasks
from handler import Handler
from db import DB
from processor import run_processor


logger = logging.getLogger('md5server')


PROJECT_PATH = pathlib.Path(__file__).absolute().parent.parent
CONFIG_PATH = PROJECT_PATH / 'config' / 'config.yml'


def setup_routes(app, handler):
    router = app.router
    router.add_get('/check', handler.check, name='check')
    router.add_post('/submit', handler.submit, name='submit')


async def init(loop, process_tasks):
    cleaners = []
    conf = load_config(CONFIG_PATH)
    server_conf = conf['server']
    redis_conf = conf['redis']
    processor_conf = conf['processor']

    app = web.Application()

    db = await DB.init(redis_conf, loop)
    cleaners.append(db.close)

    handler = Handler(db, conf)
    setup_routes(app, handler)

    host, port = server_conf['host'], server_conf['port']

    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, host, port)

    server = asyncio.create_task(site.start())
    if process_tasks:
        processor = asyncio.create_task(run_processor(processor_conf, db))
        return asyncio.gather(server, processor), cleaners
    else:
        return server, cleaners


def main(process_tasks=False):
    setup_logging()

    loop = asyncio.get_event_loop()
    task, cleaners = loop.run_until_complete(init(loop, process_tasks))

    try:
        loop.run_forever()
    except (SystemExit, KeyboardInterrupt):
        pass
    finally:
        cancel_all_tasks(loop)
        loop.run_until_complete(asyncio.gather(*[c() for c in cleaners]))
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.stop()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='A web server for MD5 hash calculation.')
    parser.add_argument('--process-tasks', '-p',
                        dest='process_tasks',
                        action='store_true',
                        help='Process requests in the same process.')
    args = parser.parse_args()
    main(args.process_tasks)

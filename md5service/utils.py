import yaml
import logging
import asyncio


def setup_logging():
    logging.basicConfig(level=logging.DEBUG)


def load_config(fname):
    with open(fname, 'rt') as f:
        data = yaml.safe_load(f)
    return data


def cancel_all_tasks(loop):
    to_cancel = {t for t in list(asyncio.Task.all_tasks(loop)) if not t.done()}
    if not to_cancel:
        return

    for task in to_cancel:
        task.cancel()

    loop.run_until_complete(
        asyncio.gather(*to_cancel, loop=loop, return_exceptions=True))

    for task in to_cancel:
        if task.cancelled():
            continue
        if task.exception() is not None:
            loop.call_exception_handler({
                'message': 'unhandled exception during asyncio.run() shutdown',
                'exception': task.exception(),
                'task': task,
            })

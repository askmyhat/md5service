import uuid
import aioredis


class DB(object):
    @classmethod
    async def init(cls, conf, loop):
        self = DB()
        self._redis = await aioredis.create_redis_pool(
            (conf['host'], conf['port']),
            minsize=conf['minsize'],
            maxsize=conf['maxsize'],
            encoding='utf-8',
            loop=loop
        )
        self.expire = conf['expire']
        return self

    async def close(self, *args, **kwargs):
        self._redis.close()
        await self._redis.wait_closed()

    async def move_running_to_pending(self):
        cur = '0'
        while cur:
            cur, tasks = await self._redis.sscan('running', cur)
            for task in tasks:
                await self._redis.lpush('pending', task)
                await self._redis.srem('running', task)

    async def create_task(self, url, email=None):
        task_id = str(uuid.uuid4())
        await self._redis.set(f'{task_id}:url', url)
        if email:
            await self._redis.set(f'{task_id}:email', email)
        await self._redis.rpush('pending', task_id)
        return task_id

    async def get_task(self):
        with await self._redis as r:
            _, task_id = await r.blpop('pending')
            await self._redis.sadd('running', task_id)
        return task_id

    async def complete_task(self, task_id, md5):
        await self._redis.set(f'{task_id}:md5', md5)
        await self._redis.expire(f'{task_id}:md5', self.expire)
        await self._redis.expire(f'{task_id}:url', self.expire)
        await self._redis.delete(f'{task_id}:email')
        await self._redis.srem('running', task_id)

    async def get_task_data(self, task_id):
        return await self._redis.mget(f'{task_id}:url',
                                      f'{task_id}:email',
                                      f'{task_id}:md5')

# Web-service for MD5 sum calculation

## Usage

### Create task

**URL** : `/submit`

**Method** : `POST`

**Parameters**:

* **url**: file location
* **email** (optional): send results when calculation is finished

**Response**:

* **id**: task id

### Get result

**URL** : `/check`

**Method** : `GET`

**Parameters**:

* **id**: task id

**Response**:

* **status**: `not found`, `running`, `failed` or `done`
* **md5**: not present if **status** is not `done`
* **url**: not present if **status** is not `done`

Example:

```
# curl -X POST -d
"email=user@example.com&url=http://site.com/file.txt"
http://localhost:8000/submit
{"id":"0e4fac17-f367-4807-8c28-8a059a2f82ac"}

# curl -X GET http://localhost:8000/check?id=0e4fac17-f367-
4807-8c28-8a059a2f82ac
{"status":"running"}

# curl -X GET http://localhost:8000/check?id=0e4fac17-f367-
4807-8c28-8a059a2f82ac
{"md5":"f4afe93ad799484b1d512cc20e93efd1","status":"done","url":"
http://site.com/file.txt"}
```

## Deploy with Docker

Requirements:

* docker>=19.03
* docker-compose>=1.24.1

Running:

```
docker-compose up
```

## Deploy with virtual environment

Requirements:

* Redis
* python 3.6+

Installation:

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

Configuration:

Configure Redis and email in `config/config.yml`

Running:

Web service and task processor in one process

```
python md5service
```

Web service and task processor in separate processes

```
python md5service/server.py
python md5service/processor.py
```

## Testing 

Install requirements:

```
pip install -r test-requirements.txt
```

Run tests:

```
bash test.sh
```

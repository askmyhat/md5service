FROM python:3.7-buster

WORKDIR /usr/src/app

COPY md5service md5service
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY docker-config/ ./config

CMD [ "python", "md5service" ]
